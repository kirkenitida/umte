package cz.uhk.umte_projekt

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import cz.uhk.umte_projekt.model.CreditDB
import cz.uhk.umte_projekt.model.CreditDao
import kotlinx.android.synthetic.main.description_credit.*

class CreditDescription : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.description_credit)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val creditId: Int = intent.getIntExtra("id", 0)
        val credit: CreditDB? = CreditDao().selectById(creditId)

        if (credit != null) {
            fillCreditDescription(credit)
            deleteButtonForCredit.setOnClickListener {
                CreditDao().delete(credit)
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun fillCreditDescription(credit: CreditDB) {
        textViewOfTypeOfCredit.text = credit.creditName
        dateTextView.text = credit.date
        textViewDescriptionOfCredit.text = credit.description
    }

}