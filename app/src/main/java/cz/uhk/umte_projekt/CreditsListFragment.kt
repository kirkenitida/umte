package cz.uhk.umte_projekt

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import cz.uhk.umte_projekt.model.CreditDB
import cz.uhk.umte_projekt.model.CreditDao
import cz.uhk.umte_projekt.model.SubjectDao
import kotlinx.android.synthetic.main.fragment_credit_list.*

class CreditsListFragment() : Fragment() {

    val dbList = mutableListOf<CreditDB>()
    var subjectId: Int = 0
    val adapter = Adapter()

    @SuppressLint("ValidFragment")
    constructor(subjectId: Int) : this() {
        this.subjectId=subjectId
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_credit_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val subject = SubjectDao().selectById(subjectId)
        if (subject != null) {
            dbList.addAll(subject.oneToManyCredits())
        }

        recyclerViewSubject.adapter = adapter
        recyclerViewSubject.layoutManager = LinearLayoutManager(context)
    }

    inner class Adapter : RecyclerView.Adapter<Adapter.Holder>() {

        override fun onCreateViewHolder(root: ViewGroup, p1: Int): Holder {
            return Holder(
                LayoutInflater.from(context)
                    .inflate(R.layout.row_credit, root, false)
            )
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.onBind()
        }

        override fun getItemCount(): Int {
            return dbList.size
        }

        inner class Holder(val item: View) : RecyclerView.ViewHolder(item),
            View.OnClickListener,
            View.OnLongClickListener {

            init {
                item.setOnClickListener(this)
                item.setOnLongClickListener(this)
            }

            fun onBind() {
                val creditNameTextView = item.findViewById<TextView>(R.id.nameCreditTextView)
                val creditIsDone = item.findViewById<CheckBox>(R.id.isDoneCheckbox)
                val credit = dbList[layoutPosition]

                creditNameTextView.text = credit.creditName
                creditIsDone.isChecked = credit.done
            }

            override fun onClick(v: View?) {
                val credit: CreditDB = dbList[layoutPosition]
                val intent = Intent(this@CreditsListFragment.context, CreditDescription::class.java)
                intent.putExtra("id", credit.id)
                startActivityForResult(intent,400)
            }

            override fun onLongClick(v: View?): Boolean {
                val credit: CreditDB = dbList[layoutPosition]
                credit.done = !credit.done
                CreditDao().createOrUpdateCredit(credit)
                val creditIsDone = item.findViewById<CheckBox>(R.id.isDoneCheckbox)
                creditIsDone.isChecked = credit.done
                return true
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 400 && resultCode == Activity.RESULT_OK) {
            dbList.clear()
            val subject = SubjectDao().selectById(subjectId)
            if (subject != null) {
                dbList.addAll(subject.oneToManyCredits())
            }
            adapter.notifyDataSetChanged()
        }
    }

}