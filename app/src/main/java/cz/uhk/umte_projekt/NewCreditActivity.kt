package cz.uhk.umte_projekt

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import cz.uhk.umte_projekt.model.CreditDB
import cz.uhk.umte_projekt.model.CreditDao
import cz.uhk.umte_projekt.model.SubjectDao
import kotlinx.android.synthetic.main.credit_new.*
import java.text.SimpleDateFormat
import java.util.*

class NewCreditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.credit_new)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        calendarViewOfCredit.setOnDateChangeListener { view, year, month, dayOfMonth ->
            calendarViewOfCredit.date = Date(year - 1900, month, dayOfMonth).time
        }

        addButtonForNewCredit.setOnClickListener {
            addNewCreditToDB(intent.getIntExtra("subjectId", 0))
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SimpleDateFormat")
    private fun addNewCreditToDB(subjectId: Int) {
        val sdf = SimpleDateFormat("dd.MM.yyyy")
        val subject = SubjectDao().selectById(subjectId)

        val credit = CreditDB()
        credit.subject = subject
        credit.creditName = editTextTypeOfCredit.text.toString()

        val selectedDate = sdf.format(calendarViewOfCredit.date)
        credit.date = selectedDate
        credit.description = editTextDescriptionOfCredit.text.toString()
        CreditDao().createOrUpdateCredit(credit)
    }

}