package cz.uhk.umte_projekt

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import cz.uhk.umte_projekt.model.CreditDB
import cz.uhk.umte_projekt.model.SubjectDB
import cz.uhk.umte_projekt.model.SubjectDao
import kotlinx.android.synthetic.main.subject_detail.*


class DetailSubject: AppCompatActivity() {
    private var subjectId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.subject_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        subjectId = intent.getIntExtra("id",0)
        val subject: SubjectDB? = SubjectDao().selectById(subjectId)
        if (subject != null){
            if (savedInstanceState == null) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainerSubjectDetail, CreditsListFragment(subjectId))
                    .commitAllowingStateLoss()
            }
            title = subject.name
            switchButtonSet(subject)
        }

        addCreditButton.setOnClickListener {
            val intent = Intent(this, NewCreditActivity::class.java)
            intent.putExtra("subjectId", subjectId)
            startActivityForResult(intent,300)
        }

    }

    private fun switchButtonSet(subject: SubjectDB) {
        switchForInterviewExam.isChecked = subject.talkExam
        switchForProjectForExam.isChecked = subject.projectExam
        switchForWriteExam.isChecked = subject.writtenExam

        switchForInterviewExam.setOnClickListener {
            subject.talkExam = switchForInterviewExam.isChecked
            SubjectDao().createOrUpdate(subject)
        }

        switchForProjectForExam.setOnClickListener {
            subject.projectExam = switchForProjectForExam.isChecked
            SubjectDao().createOrUpdate(subject)
        }

        switchForWriteExam.setOnClickListener {
            subject.writtenExam = switchForWriteExam.isChecked
            SubjectDao().createOrUpdate(subject)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 300 && resultCode == Activity.RESULT_OK) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainerSubjectDetail, CreditsListFragment(subjectId))
                .commitAllowingStateLoss()
        }
    }
}