package cz.uhk.umte_projekt

import android.app.Application
import com.raizlabs.android.dbflow.config.FlowManager

class UmteApp : Application() {

    override fun onCreate() {
        super.onCreate()
        FlowManager.init(this)
    }
}