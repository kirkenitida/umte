package cz.uhk.umte_projekt

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import android.view.MenuItem
import cz.uhk.umte_projekt.model.SubjectDB
import cz.uhk.umte_projekt.model.SubjectDao
import kotlinx.android.synthetic.main.subject_new.*


class NewSubject : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.subject_new)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addSubjectButton.setOnClickListener{
            addNewSubjectToDB()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addNewSubjectToDB(){
        val subject = SubjectDB()
        subject.name = nameEditText.text.toString()
        subject.shortcutOfSubject = shortcutEditText.text.toString()
        SubjectDao().createOrUpdate(subject)
        setResult(RESULT_OK)
        finish()
    }

}