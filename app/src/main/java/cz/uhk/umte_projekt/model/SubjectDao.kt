package cz.uhk.umte_projekt.model

import com.raizlabs.android.dbflow.sql.language.Select

class SubjectDao  {

    fun createOrUpdate(subject: SubjectDB) {
        subject.save()
    }

    fun delete(subject: SubjectDB) {
        subject.delete()
    }

    fun selectAll(): List<SubjectDB> {
        return Select()
            .from(SubjectDB::class.java)
            .queryList()
    }

    fun selectById(id: Int): SubjectDB? {
        return Select()
            .from(SubjectDB::class.java)
            .where(SubjectDB_Table.id.eq(id))
            .querySingle()
    }
}