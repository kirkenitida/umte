package cz.uhk.umte_projekt.model

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.OneToMany
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.sql.language.SQLite
import com.raizlabs.android.dbflow.structure.BaseModel


@Table(name = "subject", database = AppDatabase::class)
class SubjectDB : BaseModel() {

    @PrimaryKey(autoincrement = true)
    var id: Int? = 0

    @Column
    var name: String? = null

    @Column
    var shortcutOfSubject: String? = null

    @Column(getterName = "getTalkExam")
    var talkExam: Boolean = false

    @Column(getterName = "getWrittenExam")
    var writtenExam: Boolean = false

    @Column(getterName = "getProjectExam")
    var projectExam: Boolean = false

    var credits: List<CreditDB>? = null


    @OneToMany(methods = [OneToMany.Method.ALL], variableName = "credits")
    fun oneToManyCredits(): List<CreditDB> {

        if (credits == null) {
            credits = SQLite.select()
                .from(CreditDB::class.java)
                .where(CreditDB_Table.subject_id.eq(id))
                .queryList()
        }

        return credits as List<CreditDB>

    }
}