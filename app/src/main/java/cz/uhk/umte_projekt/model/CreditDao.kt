package cz.uhk.umte_projekt.model
import com.raizlabs.android.dbflow.sql.language.Select

class CreditDao {

    fun createOrUpdateCredit(credit: CreditDB) {
        credit.save()
    }

    fun delete(credit: CreditDB) {
        credit.delete()
    }

    fun selectAll(): List<CreditDB> {
        return Select()
            .from(CreditDB::class.java)
            .queryList()
    }

    fun selectById(id: Int): CreditDB? {
        return Select()
            .from(CreditDB::class.java)
            .where(CreditDB_Table.id.eq(id))
            .querySingle()
    }

    fun selectFiveSortedByDate(): List<CreditDB>{
        return Select()
            .from(CreditDB::class.java)
            .where(CreditDB_Table.done.`is`(false))
            .orderBy(CreditDB_Table.date,true)
            .limit(5)
            .queryList()
    }
}