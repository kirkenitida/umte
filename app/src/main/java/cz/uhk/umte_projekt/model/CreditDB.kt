package cz.uhk.umte_projekt.model

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.ForeignKey
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel


@Table(name = "credit", database = AppDatabase::class)
class CreditDB : BaseModel() {

    @PrimaryKey(autoincrement = true)
    var id: Int? = 0

    @Column
    var creditName: String? = null

    @Column
    var date: String? = null

    @Column
    var description: String? = null

    @ForeignKey(stubbedRelationship = true)
    var subject: SubjectDB? = null

    @Column(getterName = "getDone")
    var done: Boolean = false

}