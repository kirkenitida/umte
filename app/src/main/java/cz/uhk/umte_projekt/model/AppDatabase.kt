package cz.uhk.umte_projekt.model

import com.raizlabs.android.dbflow.annotation.Database

@Database(
    version = AppDatabase.VERSION,
    name = AppDatabase.NAME
)
object AppDatabase {

    const val VERSION = 3
    const val NAME = "UmteAppDB"
}