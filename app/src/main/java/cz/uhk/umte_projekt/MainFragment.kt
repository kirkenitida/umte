package cz.uhk.umte_projekt

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cz.uhk.umte_projekt.model.SubjectDB
import cz.uhk.umte_projekt.model.SubjectDao
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {

    val dbList = mutableListOf<SubjectDB>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dbList.addAll(SubjectDao().selectAll())

        val adapter = Adapter()
        recyclerViewMain.adapter = adapter
        recyclerViewMain.layoutManager = GridLayoutManager(context, 2)
    }

    inner class Adapter : RecyclerView.Adapter<Adapter.Holder>() {

        override fun onCreateViewHolder(root: ViewGroup, p1: Int): Holder {
            return Holder(
                LayoutInflater.from(context)
                    .inflate(R.layout.box_subject, root, false)
            )
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.onBind()
        }

        override fun getItemCount(): Int {
            return dbList.size
        }

        inner class Holder(val item: View) : RecyclerView.ViewHolder(item),
            View.OnClickListener,
            View.OnLongClickListener {

            init {
                item.setOnClickListener(this)
                item.setOnLongClickListener(this)
            }

            fun onBind() {
                val shortcut = item.findViewById<TextView>(R.id.subjectShortcutTextView)
                val subject = dbList[layoutPosition]

                shortcut.text = subject.shortcutOfSubject
            }

            override fun onClick(v: View?) {
                val subject: SubjectDB = dbList[layoutPosition]
                val intent = Intent(this@MainFragment.context, DetailSubject::class.java)
                intent.putExtra("id", subject.id)
                startActivityForResult(intent,200)
            }

            override fun onLongClick(v: View?): Boolean {
                val subject = dbList[layoutPosition]
                SubjectDao().delete(subject)
                dbList.removeAt(layoutPosition)
                notifyItemRemoved(layoutPosition)
                (activity as MainActivity).callback()
                return true
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 200 && resultCode == Activity.RESULT_CANCELED) {
            (activity as MainActivity).callback()
        }
    }
}