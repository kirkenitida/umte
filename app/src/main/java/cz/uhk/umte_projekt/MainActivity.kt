package cz.uhk.umte_projekt

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.ListView
import cz.uhk.umte_projekt.model.CreditDB
import cz.uhk.umte_projekt.model.CreditDao
import cz.uhk.umte_projekt.model.SubjectDB
import cz.uhk.umte_projekt.model.SubjectDao
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prepareListAdapter()

        openNewSubject.setOnClickListener {
            val intent = Intent(this, NewSubject::class.java)
            startActivityForResult(intent, 100)
        }


        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainerMain, MainFragment())
                .commitAllowingStateLoss()
        }
    }


    private fun prepareListAdapter() {
        val nextActivities: List<CreditDB> = CreditDao().selectFiveSortedByDate()

        val data = ArrayList<String>()
        for (activity in nextActivities) {
            val subject: SubjectDB? = SubjectDao().selectById(activity.subject!!.id!!)
            val activityString: String =
                String.format(
                    "%s | %s | %s",
                    subject!!.shortcutOfSubject,
                    activity.creditName,
                    activity.date
                )
            data.add(activityString)
        }

        val listView = findViewById<ListView>(R.id.listActivities)
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, data)
        listView.adapter = arrayAdapter
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainerMain, MainFragment())
                .commitAllowingStateLoss()
        }
    }

    fun callback(){
        prepareListAdapter()
    }

}
